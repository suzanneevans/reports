DECLARE @start_date datetime, @end_date datetime

SET @start_date = case when @last_run_date < dateadd(day, -7, current_timestamp) then @last_run_date else  dateadd(day, -7, current_timestamp) end

SELECT member_account_type as [Classification]
	  ,md.[Member_Reference] as 'Member Reference'
      ,[Member_Id] as AtunePlanMemberID
	  ,m.plan_member_ref as MemberReferenceWithLeading0
      ,md.Title as 'Title'
      ,[Given_Names] as 'Given Names'
      ,[Surname]
      ,format(account_closed_date,'dd/MM/yyyy') as 'Exited Date'
FROM [MESQ02\SQL2014].[Staging_AAS_Empirics_2016].[dbo].[Member] m
  join [Member_Details] md on plan_member_ref collate Latin1_General_CI_AI = md.member_reference
  where (member_account_type = 'KINE Emp Sponsored' or member_account_type = 'KINE Personal') 
  and (account_closed_date > @start_date)
order by md.member_reference