DECLARE @start_date datetime, @end_date datetime

SET @start_date = case when @last_run_date < dateadd(day, -7, current_timestamp) then @last_run_date else  dateadd(day, -7, current_timestamp) end
SET @end_date = current_timestamp

SELECT member_account_type as [Classification]
	  ,md.[Member_Reference] as 'Member Reference'
      ,[Member_Id] as AtunePlanMemberID
	  ,m.plan_member_ref as MemberReferenceWithLeading0
      ,format(m.joined_fund_date,'dd/MM/yyyy') as 'Joined Fund Date'
      ,[Member_Status_Source] as 'Member Active Status'
      ,m.[Gender]
      ,md.Title as 'Title'
      ,[Given_Names] as 'Given Names'
      ,[Surname]
      ,[Res_Address_Line]
      ,[Res_Suburb]
      ,[Res_State]
      ,[Res_Postcode]
      ,[Res_Country]
      ,[Res_Address_Indicator]
      ,[Postal_Address_Line]
      ,[Postal_Suburb]
      ,[Postal_State]
      ,[Postal_Postcode]
      ,[Postal_Country]
      ,[Postal_Address_Indicator]
	  --,Addresses -- from Empirics
      ,format([Date_Of_Birth],'dd/MM/yyyy') as 'Date Of Birth'
      ,m.[Member_Age]
      ,m.[Email_Address]
      ,[phone_mobile_no]
      ,[phone_no]
	  ,[balance_amount]
      ,m.[Privacy_Consent]
      ,case when [Marketing_Email_Opt_Out] = 'Do Not Email' then 'NO' ELSE 'YES' END as 'Email Marketing Flag'
      ,case when [Marketing_SMS_Opt_Out] = 'Do Not SMS' then 'NO' ELSE 'YES' END as 'SMS Marketing Flag'
      ,case when [Marketing_DM_Opt_Out] = 'Do Not DM' then 'NO' ELSE 'YES' END as 'Direct Marketing Mail flag'
      ,case when [Marketing_Phone_Opt_Out] = 'Do Not Phone' then 'NO' ELSE 'YES' END as 'Telemarketing Flag'
      ,case when m.[Web_Profile] = 'No Web Profile' then 'No' ELSE 'Yes' END as 'Web Profile'
      ,m.[eStatement]
	   ,format(case 
        when coalesce([Web_Profile_Create_Date],[Web_Last_login_Date]) = '0' then null
        when isnumeric(left(coalesce([Web_Profile_Create_Date],[Web_Last_login_Date]),4)) = 1 then cast(coalesce([Web_Profile_Create_Date],[Web_Last_login_Date]) as date) 
        when len(coalesce([Web_Profile_Create_Date],[Web_Last_login_Date])) = 27 then convert(date, coalesce([Web_Profile_Create_Date],[Web_Last_login_Date]), 109) 
        when len(coalesce([Web_Profile_Create_Date],[Web_Last_login_Date])) = 19 then convert(date, coalesce([Web_Profile_Create_Date],[Web_Last_login_Date]), 100) 
        when len(coalesce([Web_Profile_Create_Date],[Web_Last_login_Date])) = 8 then convert(date, coalesce([Web_Profile_Create_Date],[Web_Last_login_Date]), 112) 
    end ,'dd/MM/yyyy') as 'Web Created'
	   ,format(case 
        when [Web_Last_login_Date] = '0' then null
        when isnumeric(left([Web_Last_login_Date],4)) = 1 then cast([Web_Last_login_Date] as date) 
        when len([Web_Last_login_Date]) = 19 then convert(date, [Web_Last_login_Date], 100) 
        when len([Web_Last_login_Date]) = 27 then convert(date, [Web_Last_login_Date], 109) 
        when len([Web_Last_login_Date]) = 8 then convert(date, [Web_Last_login_Date], 112) 
    end ,'dd/MM/yyyy') as 'Web Login'
      ,mf.[TFN_Status]
      ,mf.[Lost_Super_Consent]
      ,mf.[Date_Lost_Consent_Given]
	  ,mt.TFN
FROM [MESQ02\SQL2014].[Staging_AAS_Empirics_2016].[dbo].[Member] m
  join [Member_Details] md on right(plan_member_id,len(plan_member_id)-1) collate Latin1_General_CI_AI = md.Member_Id
  join [Member_Address] ma on md.Member_Reference = ma.Member_Reference
  join Member_Flags mf on md.Member_Reference = mf.Member_Reference
  left join Member_TFN mt on md.Member_Reference = mt.Member_Reference
  where [Account_Opened_Date] between @start_date and @end_date
  and (member_account_type = 'KINE Emp Sponsored' or member_account_type = 'KINE Personal') 
  and (account_closed_date is null or datediff(day, account_opened_date, account_closed_date) > 10)
order by md.member_reference