DECLARE @start_date DATE, 
        @end_date   DATE 
SET @start_date = CAST(DATEADD(D, (DATEPART(D, DATEADD(m, -12, GETDATE())) * -1) + 1, DATEADD(m, -12, GETDATE())) AS date)
SET @end_date = DATEADD(s, -1, DATEADD(mm, DATEDIFF(m, 0, GETDATE()), 0))

print @start_date
print @end_date



IF OBJECT_ID('tempdb..#MonthEnds') IS NOT NULL
  DROP TABLE #MonthEnds
CREATE TABLE #MonthEnds (
  bomonth date,
  EOMonthDate date,
  MonthDate varchar(10),
  MonthEnd varchar(10) COLLATE Latin1_General_CI_AS
)

DECLARE @cnt int = 0;
DECLARE @curr_month date;
WHILE @cnt < 12
BEGIN
  SET @curr_month = DATEADD(MONTH, @cnt, @start_date)
  INSERT INTO #MonthEnds
    SELECT
      @curr_month,
      eomonth(@curr_month),
      format(@curr_month, 'yyyyMM'),
      format(@curr_month, 'MMM-yy') AS MonthEnd
  SET @cnt = @cnt + 1;
END;

DECLARE @CurrentZero integer
SELECT @CurrentZero = COUNT(*)
FROM [MESQ02\SQL2014].[Staging_AAS_Empirics_2016].[dbo].[Member]
WHERE Member_Status = 'Open'
AND Member_Account_Type IN ('KINE Emp Sponsored', 'KINE Personal')
AND balance_amount < 0.01

IF OBJECT_ID('tempdb..#counts') IS NOT NULL DROP TABLE #counts
SELECT
  me.MonthDate,
  MonthEnd,
  SUM(tots.MemberCount) - COALESCE(SUM(z2.MemberCount), @CurrentZero) AS 'Actual Member Count'
  ,SUBSTRING(me.monthdate, 5, 6) + SUBSTRING(me.monthdate, 1, 4) as data_sql_index
  ,'fst_smratio' + SUBSTRING(me.monthdate, 5, 6) + SUBSTRING(me.monthdate, 1, 4) as comment_sql_index
INTO #counts
FROM #MonthEnds me
LEFT JOIN (SELECT me.MonthDate,COUNT(*) AS MemberCount
	FROM #MonthEnds me
	JOIN [MESQ02\SQL2014].[Staging_AAS_Empirics_2016].[dbo].[Member] m ON (m.Account_Opened_Date < me.EOMonthDate AND (Member_Status = 'Open' OR account_closed_date > me.EOMonthDate))
	WHERE Member_Account_Type IN ('KINE Emp Sponsored', 'KINE Personal')
	GROUP BY me.MonthDate
) tots ON me.MonthDate = tots.MonthDate
LEFT JOIN (
	SELECT me.MonthDate, COUNT(*) AS MemberCount
	FROM #MonthEnds me
	JOIN [MESQ02\SQL2014].[Staging_AAS_Empirics_2016].[dbo].[Member_snapshot] ms ON ms.Effective_Date = DATEADD(MONTH, 1, me.bomonth) AND ms.Balance < 0.01
	GROUP BY me.MonthDate
) z2 ON me.MonthDate = z2.MonthDate
GROUP BY me.MonthDate, MonthEnd


select monthdate
	,monthend
	,cast(cast([Actual Member Count] as decimal(10,2))/5000/dp.[# FTE staff (incl. FTC)] as decimal(5,2)) as FTE_Member
	,a.sql_Index
	,a.date
	,FORMAT(a.date,'MMM-yyyy') 'MonthYear'
	,a.Act as ActHigh
	,a.Decide as DecideHigh
	,a.Monitor as MonitorHigh
	,a.Act2 as ActLow
	,a.Decide2 as DecideLow
	,a.Monitor2 as MonitorLow
	,a.Rating
	,a.[Rag Commentary]
	,a.[Management Commentary]
	,ROW_NUMBER() OVER (ORDER BY (SELECT 12)) AS RowNumber
from #counts c
join [KS_Staging].[dbo].[Salesforce_KRIDataPoints] dp WITH (NOLOCK) on c.data_sql_index = dp.sql_index collate Latin1_General_CI_AS 
JOIN [KS_Staging].[dbo].[Salesforce_KRIRatings] a WITH (NOLOCK) ON a.sql_Index = c.comment_sql_index collate Latin1_General_CI_AS 
ORDER BY MonthDate DESC