--- first name of the member 
--- last name of the member 
--- member number 
--- member email address 
--- member mobile phone number 
--- the activity undertaken by the member 
--- outbound call (MEACs) 
--- interacted with MOS 
--- activities undertaken in MOS 
--- inbound/outbound (MEACs) consultant id 
--- inbound/outbound (MEACs) consultant name

DECLARE @start_date date

SET @start_date = dateadd(day,-3,current_timestamp)

select dateadd(day,-3,current_timestamp)
select SUBSTRING(m.[Plan_Member_Ref], PATINDEX('%[^0]%', m.[Plan_Member_Ref]+'.'), LEN(m.[Plan_Member_Ref])) as MemberReference
--,m.first_name, m.Last_Name, 
,left(cd.Source_First_Name, coalesce(nullif(charindex(' ', cd.Source_First_Name),0),len(cd.source_first_name))) as FirstName
,cd.Source_Last_Name
,m.Email_Address, m.[Phone_Mobile_No], LastLogonDate
from member m
join Contact_Details cd on m.Plan_Member_ID = cd.Plan_Member_ID and cd.Contact_Type = 'Primary'
join (
	select plan_member_id, max(action_date) as LastLogonDate 
	from web_tracking w 
	where Action_Date >= @start_date
	group by Plan_Member_ID
) w on m.plan_member_ID = w.Plan_Member_ID
order by MemberReference