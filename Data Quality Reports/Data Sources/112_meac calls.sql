DECLARE @start_date date

SET @start_date = dateadd(day,-3,current_timestamp)
IF OBJECT_ID('tempdb..#MeacCalls') IS NOT NULL DROP TABLE #MeacCalls
select 
 member_reference, campaign_code, contact_outcome
, case when mos_ind = 1 then ', MOS' else '' end as MOS_Act
, case when portability_ind = 1 then ', Portability' else '' end as Port_Act
, case when beneficiary_ind = 1 then ', Beneficiaries' else '' end as Ben_Act
, case when contributions_ind = 1 then ', Contributions' else '' end as Cont_Act
, case when insurance_ind = 1 then ', Insurance' else '' end as Ins_Act
, case when consolidation_ind = 1 then ', Consolidation' else '' end as Cons_Act
, case when investments_ind = 1 then ', Investments' else '' end as Inv_Act
, case when pension_ind = 1 then ', Pensions' else '' end as Pens_Act
, case when uk_pension_ind = 1 then ', UK Pensions' else '' end as UKPens_Act
,salesforce_last_activity_date, salesforce_created_date, ownerid, substring(owner_name, charindex('>', Owner_Name)+1, (charindex('</a>', Owner_Name) - charindex('>', Owner_Name)-1)) as OwnerName
into #MeacCalls
from KS_staging.dbo.Salesforce_Member_Opportunity
where (Salesforce_Last_Activity_Date >= @start_date or Salesforce_Created_Date >= @start_date)
and IsDeleted = 0
		and	(Contact_Outcome like 'Contacted%'	OR 
			Contact_Outcome like 'General%'		OR 
			Contact_Outcome like 'Intra%'		OR 
			Contact_Outcome like 'Personal%'    OR
			Contact_Outcome like 'Factual%' 
		) 
and member_reference is not null

--The data points required are: 
--- first name of the member 
--- last name of the member 
--- member number 
--- member email address 
--- member mobile phone number 
--- the activity undertaken by the member 
--- inbound/outbound (MEACs) consultant id 
--- inbound/outbound (MEACs) consultant name


select member_reference
,left(cd.Source_First_Name, coalesce(nullif(charindex(' ', cd.Source_First_Name),0),len(cd.source_first_name))) as FirstName
,cd.Source_Last_Name,m.email_address, m.[Phone_Mobile_No]
,campaign_code, contact_outcome, MOS_Act + Port_Act + Ben_Act + Cont_Act + Ins_Act + Cons_Act + Inv_Act + Pens_Act + UKPens_Act as DiscussionList
,salesforce_last_activity_date, salesforce_created_date, OwnerName
from #MeacCalls mc
left join [MESQ02\SQL2014].[Staging_AAS_Empirics_2016].[dbo].member m on mc.Member_Reference collate Latin1_General_CI_AI = SUBSTRING([Plan_Member_Ref], PATINDEX('%[^0]%', [Plan_Member_Ref]+'.'), LEN([Plan_Member_Ref])) and m.plan_member_ref <> 'Restricted'
join [MESQ02\SQL2014].[Staging_AAS_Empirics_2016].[dbo].contact_Details cd on cd.plan_member_id = m.plan_member_id and cd.contact_type = 'Primary'
